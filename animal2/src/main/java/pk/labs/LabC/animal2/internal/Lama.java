/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal2.internal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;
/**
 *
 * @author st
 */
public class Lama implements Animal {
    
    private String status;
    private PropertyChangeSupport change;
    
    public Lama()
    {
        change = new PropertyChangeSupport(this);
    }

    @Override
    public String getSpecies() {
        return "Lama";
    }

    @Override
    public String getName() {
        return "Kuzko";
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        change.firePropertyChange("status", this.status, status);
        this.status = status;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        change.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        change.removePropertyChangeListener(listener);
    }
    
}
