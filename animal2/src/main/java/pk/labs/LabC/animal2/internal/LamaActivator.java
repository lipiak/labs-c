/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal2.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.logger.Logger;

/**
 *
 * @author st
 */
public class LamaActivator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
        
        bc.registerService(Lama.class.getName(), new Lama(), null);
        System.out.println("Przychodzi Lama");
        Logger.get().log(this,"startLama");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        System.out.println("Odchodzi Pies");
        Logger.get().log(this,"stopLama");
    }
    
}
